# Zara.com Front-End Test

## Descripción

- Para realizar el test he utilizado React como framework.
- Para crear la aplicación he utilizado el generador `create-react-app` que provee la funcionalidad directamente para ser utilizada.
- He decido por simplicidad utilizar `Javascript`, pero soy partidario de `Typescript` y de su checkeo de tipos.
- En tema de estilos he escogido `SASS`, pero he utilizado modules `CSS-in-JS` en otros proyectos.
- Como state management he escogido Context API + Hooks, pero he realizado applicaciones con otras librerias como `Redux`(React), `VueX`(Vue) o `NgRx`(Angular). Todos tienen la misma pattern.
- Para los test unitarios he utilizado `Jest` y `Enzyme`.
- En terminos de Git flow, he hecho los commits directamente en `master`. Normalmente utilizo [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) y rebasing.

## Versiones

He creado un [Changelog](https://bitbucket.org/nandobermejo/test-zara/src/master/CHANGELOG.md) donde he documentado las versiones mediante una pequeña descripcion.


## Directorio

He creado la siguiente estructura para la aplicación:
```
/public
/src
  /components
  /containers
  /context
  /helpers
  /styles 
  /views
```

## API

Para simplificar y normalizar los datos del feed, he creado un wrapper API, también para solventar el problema de CORS actuando como un proxy.


#### Podcast List

Devuelve un listado de podcast

Request:
```
GET /podscast
```

Response:
200
```
Array<PodcastList>
```
500
```
Error
```

Model:
```
PodcastList {
  id        uuid
  title     string
  artist    string
  imageUrl  string
}

Error {
  message       string
  error         Error
}
```

### Podcast List

Devuelve la informacion de un podcast

Request
```
GET /podscast/:podcastId
```

Response:

200
```
Podcast
```
500
```
Error
```

Models:

```
Podcast {
  id            uuid
  title         string
  artist        string
  imageUrl      string
  description   string
  episodes      Array<Episode>
}

Episode {
  id            uuid
  title         string
  audioUrl      string
  audioType     string
  description   string
  duration      string
  date          datetime
}

Error {
  message       string
  error         Error
}
```


## Scripts disponibles

En la carpeta del proyecto, puedes ejecutar los siguientes commandos:

### `npm start`

Lanza la applicación en modo de desarrollo.

Abre [http://localhost:3000](http://localhost:3000)

> Nota: 
>
> Hay que hacer el run del commando `npm run proxy` simultáneamente para lanzar la API

### `npm run proxy`

Lanza un servidor que recrea una API con un proxy

### `npm test`

Lanza los test en modo interactivo.

### `npm run build`

Hace el build de la applicación en modo producción.
