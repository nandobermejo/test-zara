# Changelog

## v0.1.0
- Version initial.

## v0.2.0
- Crear la estructura de los containers.
- Hacer la configuración del Test framework.

## v0.3.0
- Añadir las dependencias del Router y su configuración.
- Crear estructura para las views y su implementación inicial.

## v0.4.0
- Crear el state management mediante React Context and Hooks.

## v0.5.0        
- Crear el component PodcastCard.
- Crear el component PodcastSideBar.
- Añadir la navigación mediante un link desde un elemento de la lista a los detalles del mismo.
- Actualizar el Grid de la vista de los detalles.

## v0.6.0
- Añadir podcast API endpoint para incluir los datos proporcionados mediante la feedUrl.
- Parsear los datos recibidos de RSS+XML a JSON.
- Actualizar el componente PodcastSidebar para utilizar los nuevos datos del podcast.
- Crear el componente EpisodeList para mostrar la lista de los episodios de un podcast.

## v0.7.0
- Implementación de la arquitectura Redux para el stage management mendiante Context API y Hooks.

## v0.8.0
- Completar toda la navegación entre las diferentes vistas.

## v0.9.0
- Crear el componente EpisodeCard para mostrar la información del episodio y su reproductor de audio.

## v0.10.0
- Implementación del filtrado de la lista de podcast
- Crear el componente Message.
- Crear el componente Counter.
- Crear el componente InputSearch.

## v0.11.0
- Crear el LocalStorage helper para la manipulación de la caché
- Crear una caché en la applicación para reducir la frecuencia de las peticiones de datos.

## v0.12.0
- Crear un indicador para mostrar que la aplicación está en su primera interación.

## v0.14.0
- Manejo de errores.
- Crear modelos y entidades en el servidor proxy para normalizarlos.
- Actualización de las propiedades de los objectos a renderizar.

## v0.15.0
- Create el archivo CHANGELOG
- Actualizar la información del README

## v0.16.0
- Arreglar error tipográfico en el archivo README
