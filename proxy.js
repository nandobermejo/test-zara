const express = require('express');
const fetch = require('node-fetch');
const parser = require('xml2js');
const { get } = require('lodash');
const shortid = require('shortid');
const app = express();
const PORT = 3001;
const FEED_URL = 'https://itunes.apple.com';

class PodcastList {
  constructor(data){
    this.id = get(data,'id.attributes["im:id"]');
    this.title = get(data,'["im:name"].label');
    this.artist = get(data,'["im:artist"].label');
    this.imageUrl = get(data,'["im:image"][0].label');
  }
}

class PodcastItem {
  constructor(data){
    this.id = get(data,'id');
    this.title = get(data,'title');
    this.artist = get(data,'["itunes:author"]');
    this.imageUrl = get(data,'image.url');
    this.description = get(data,'description');
    this.episodes = get(data,'item').map((item) => new Episode(item));
  }
}

class Episode {
  constructor(data){
    this.id = shortid.generate();
    this.title = get(data,'title');
    this.audioUrl = get(data,'enclosure.$.url');
    this.audioType = get(data,'enclosure.$.type');
    this.description = get(data,'description');
    this.duration = get(data, 'itunes:duration');
    this.date = get(data,'pubDate');
  }
}

const XMLParser = async (xml) => {
  return await new Promise((resolve, reject) => parser.parseString(
    xml,
    {
      explicitArray : false
    },
    (err, result) => {
      if (err) return reject(err);
      return resolve(result.rss.channel);
    }
  ));
}

const fetchFeed = async (url) => {
  return await fetch(url)
  .then(res => res.text())
  .then(result => XMLParser(result))
  .catch(err => err);
}

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/podcasts', async (req, res, next) => {
  try{
    const data = await fetch(`${FEED_URL}/us/rss/toppodcasts/limit=100/genre=1310/json`);
    const json = await data.json();
    const list = json.feed.entry;
    const mappedList = list.map(item => new PodcastList(item));
    res.json(mappedList);
  } catch (err) {
    next(err);
  }
});

app.get('/podcasts/:podcastId', async (req, res, next) => {
  try{
    const podcastId = req.params.podcastId;
    const result = await fetch(`${FEED_URL}/lookup?id=${podcastId}`);
    const json = await result.json();
    const feedUrl = json.results[0].feedUrl;
    const feed = await fetchFeed(feedUrl);
    const mappedPodcast = new PodcastItem(feed);
    res.json(mappedPodcast);
  } catch(err) {
    next(err);
  }
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err
  });
});

app.listen(PORT, () => console.log(`Proxy is running on port http://localhost:${PORT}`))