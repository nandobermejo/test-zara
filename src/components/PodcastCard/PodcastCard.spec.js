import React from 'react';
import { shallow } from 'enzyme';

import { PodcastCard } from './PodcastCard';

const mock = {
  id: 'ID',
  title: 'TITLE',
  artist: 'ARTIST',
  imageUrl: 'IMAGE_URL',
};

describe('PodcastCard', () => {
  it('should render without crashing', () => {
    const component = shallow(<PodcastCard podcast={mock}/>);
    expect(component.find('.PodcastCard')).toHaveLength(1);
  });

  it('should render avatar image', () => {
    const component = shallow(<PodcastCard podcast={mock}/>);
    expect(component.find('.avatar')).toHaveLength(1);
  });

  it('should render the podcast title', () => {
    const component = shallow(<PodcastCard podcast={mock}/>);
    expect(component.find('.title').text()).toEqual('TITLE');
  });

  it('should render the podcast author', () => {
    const component = shallow(<PodcastCard podcast={mock}/>);
    expect(component.find('.artist').text()).toEqual('ARTIST');
  });
});



