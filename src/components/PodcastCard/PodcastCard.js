import React from 'react';

import './PodcastCard.scss';

export const PodcastCard = (props) => {
  const { podcast } = props;
  return (
    <div className="PodcastCard">
      <img className="avatar" src={podcast.imageUrl} alt=""/>
      <div className="title">{podcast.title}</div>
      <div className="artist">{podcast.artist}</div>
    </div>
  );
}