import React from 'react';

import './InputSearch.scss';

export const InputSearch = (props) => {
  const { placeholder, onChange } = props;
  return (
    <input  
      type="text" 
      className="InputSearch"
      placeholder={placeholder}
      onChange={(e) => onChange(e.target.value)}
    />
  )
}