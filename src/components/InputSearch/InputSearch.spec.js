import React from 'react';
import { shallow } from 'enzyme';

import { InputSearch } from './InputSearch';

describe('InputSearch', () => {
  const mockProps = {
    placeholder: 'PLACEHOLDER',
    onClick: jest.fn().mockImplementation(() => {})
  }

  it('should render without crashing', () => {
    const component = shallow(<InputSearch {...mockProps} />);
    expect(component.find('.InputSearch')).toHaveLength(1);
  });
});