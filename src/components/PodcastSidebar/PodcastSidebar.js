import React from 'react';

import './PodcastSidebar.scss';

export const PodcastSidebar = (props) => {
  const { podcast, onClick } = props;
  return (
    <div className="PodcastSidebar">
      <div className="link" onClick={onClick}>
        <div className="avatar">
          <img src={podcast.imageUrl} alt=""/>
        </div>
        <div className="title">{podcast.title}</div>
        <div className="artist">By {podcast.artist}</div>
      </div>
      <div className="divider"></div>
      <div className="label">Description:</div>
      <div className="description">{podcast.description}</div>
    </div>
  );
}