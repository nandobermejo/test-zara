import React from 'react';
import { shallow } from 'enzyme';

import { PodcastSidebar } from './PodcastSidebar';

const mock = {
  'image': {
    'url': 'IMAGE_TEXT'
  },
  'title': 'NAME_TEXT',
  'itunes:author': 'ARTIST_TEXT',
  'description': 'SUMMARY_TEXT'
};

describe('PodcastSidebar', () => {
  it('should render without crashing', () => {
    const component = shallow(<PodcastSidebar podcast={mock}/>);
    expect(component.find('.PodcastSidebar')).toHaveLength(1);
  });
});


