import React from 'react';
import { shallow } from 'enzyme';

import { EpisodeList } from './EpisodeList';

const mock = [
  {
    id: 'ID',
    title: 'TITLE',
    artist: 'ARTIST',
    date: new Date(),
    duration: '10:10'
  }
];

describe('EpisodeList', () => {
  it('should render without crashing', () => {
    const component = shallow(<EpisodeList list={mock}/>);
    expect(component.find('.EpisodeList')).toHaveLength(1);
  });
});


