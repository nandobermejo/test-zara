import React from 'react';

import './EpisodeList.scss';

export const EpisodeList = (props) => {
  const { list, onClick } = props;
  return (
    <div className="EpisodeList">
      <div className="episode__counter">
        <span className="text">Episodes: {list.length}</span>
      </div>
      <table>
        <thead>
          <tr>
            <th>Title</th>
            <th>Date</th>
            <th>Duration</th>
          </tr>
        </thead>
        <tbody>
          {
            list.map(item => {
              const date = new Date(item.date);
              return (
                <tr key={item.id}>
                  <td className="title" onClick={() => onClick(item.id)}>{item.title}</td>
                  <td className="date">{`${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`}</td>
                  <td className="duration">{item.duration}</td>
                </tr>
              );
            })
          }
        </tbody>
      </table>
    </div>
  )
}