import React from 'react';

import './EpisodeCard.scss';

export const EpisodeCard = (props) => {
  const { episode } = props;

  const setMarkup = (data) => {
    return {
      __html: data
    }
  }

  return (
    <div className="EpisodeCard">
      <div className="title">{episode.title}</div>
      <div className="description" dangerouslySetInnerHTML={setMarkup(episode.description)}></div>
      <audio className="audio" controls="controls">
        <source src={episode.audioUrl} type={episode.audioType} />
      </audio>
    </div>
  );
}