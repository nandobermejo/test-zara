import React from 'react';
import { shallow } from 'enzyme';

import { EpisodeCard } from './EpisodeCard';

const mock = {
  'title': 'TITLE_TEXT',
  'description': 'DESCRIPTION_TEXT',
  'enclosure': {
    '$': {
      'url': 'URL',
      'type': 'TYPE'
    }
  }
};

describe('EpisodeCard', () => {
  it('should render without crashing', () => {
    const component = shallow(<EpisodeCard episode={mock}/>);
    expect(component.find('.EpisodeCard')).toHaveLength(1);
  });
});