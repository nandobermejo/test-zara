import React from 'react';
import { shallow } from 'enzyme';

import { Message } from './Message';

describe('Message', () => {
  const mock = 'Text example';

  it('should render without crashing', () => {
    const component = shallow(<Message text={mock}/>);
    expect(component.find('.Message')).toHaveLength(1);
  });

  it('should display the correct message', () => {
    const component = shallow(<Message text={mock}/>);
    expect(component.text()).toEqual(mock);
  });
});