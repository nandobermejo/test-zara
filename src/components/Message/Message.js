import React from 'react';

import './Message.scss';

export const Message = (props) => {
  const { text } = props;
  return (
    <div className="Message">{text}</div>
  )
}