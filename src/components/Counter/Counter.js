import React from 'react';

import './Counter.scss';

export const Counter = (props) => {
  const { count } = props;
  return (
    <div className="Counter">{count}</div>
  )
}