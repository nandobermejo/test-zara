import React from 'react';
import { shallow } from 'enzyme';

import { Counter } from './Counter';

describe('Counter', () => {
  const mock = '1';

  it('should render without crashing', () => {
    const component = shallow(<Counter count={mock}/>);
    expect(component.find('.Counter')).toHaveLength(1);
  });

  it('should display the correct count', () => {
    const component = shallow(<Counter count={mock}/>);
    expect(component.text()).toEqual(mock);
  });
});