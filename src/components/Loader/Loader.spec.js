import React from 'react';
import { shallow } from 'enzyme';

import { Loader } from './Loader';

describe('Loader', () => {
  it('should render without crashing', () => {
    const component = shallow(<Loader/>);
    expect(component.find('.Loader')).toHaveLength(1);
  });
});