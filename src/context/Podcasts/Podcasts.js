import React, { useReducer, useEffect } from 'react';

import { PodcastApi, LocalStorage } from 'helpers';

const initialState = {
  list: [],
  filtered: [],
  podcast: null,
  search: null,
  podcastId: null
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'LOAD_LIST':
      return {
        ...state,
        list: action.payload,
        filtered: action.payload
      };
    case 'FILTER_LIST':
      const regex = RegExp(action.search,'i');
      return {
        ...state,
        search: action.search,
        filtered: state.list.filter((item) => regex.test(item.title) || regex.test(item.artist))
      };
    case 'SELECT_PODCAST':
      return {
        ...state,
        podcastId: action.id
      }
    case 'UNSELECT_PODCAST':
      return {
        ...state,
        podcastId: null
      }
    case 'SAVE_PODCAST':
      return {
        ...state,
        podcast: action.payload
      }
    case 'CLEAR_PODCAST':
      return {
        ...state,
        podcast: null
      }
    default:
      return state;
  }
}

export const PodcastContext = React.createContext({});

export const PodcastProvider = (props) => {
  const { children } = props;
  const [ state, dispatch ] = useReducer(reducer, initialState);

  useEffect(() => {
    const getPodcasts = async() => {
      const cache = LocalStorage.get('cache-podcasts');
      if(cache) return cache;

      return await PodcastApi.getPodcasts();
    }
    getPodcasts()
    .then(data => {
      if(data.error) return;
      LocalStorage.set('cache-podcasts', data);
      return dispatch({
        type: 'LOAD_LIST',
        payload: data
      })
    });
  }, []);

  useEffect(() => {
    if(!state.podcastId) return dispatch({ type: 'CLEAR_PODCAST' });
    const getPodcast = async(id) => {
      const cache = LocalStorage.get(`cache-podcast-${id}`);
      if(cache) return cache;

      return await PodcastApi.getPodcast(id);
    }
    getPodcast(state.podcastId)
    .then(data => {
      if(data.error) return;
      LocalStorage.set(`cache-podcast-${state.podcastId}`, data);
      return dispatch({
        type: 'SAVE_PODCAST',
        payload: data
      })
    });
  }, [state.podcastId])

  const context = {
    state,
    dispatch
  }

  return (
    <PodcastContext.Provider value={context}>
      {children}
    </PodcastContext.Provider>
  );
}


