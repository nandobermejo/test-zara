import React from 'react';
import { shallow } from 'enzyme';

import { PodcastProvider } from 'context';

describe('PodcastProvider', () => {
  it('should render without crashing', () => {
    const children = <div>children</div>;
    const component = shallow(
      <PodcastProvider>{children}</PodcastProvider>
    );
    expect(component.text()).toEqual('children');
  });
})

