import React, { useContext, useEffect, useState } from 'react';
import { Redirect } from 'react-router';

import './Episodes.scss';
import { PodcastContext } from 'context';
import { PodcastSidebar, EpisodeCard, Loader } from 'components';

export const Episodes = (props) => {
  const { podcastId, episodeId } = props.match.params;
  const { state, dispatch } = useContext(PodcastContext);
  const [ redirect, setRedirect ] = useState();

  useEffect(() => {
    dispatch({
      type: 'SELECT_PODCAST',
      id: podcastId
    });
    return () => dispatch({ type: 'UNSELECT_PODCAST'});
  }, [podcastId, dispatch]);

  if(!state.podcast) return <Loader />
  if(redirect) return <Redirect to={`/podcast/${podcastId}`} />

  const episode = state.podcast.episodes.find(item => item.id === episodeId);

  return (
    <div className="Episodes">
      <div className="Episodes__Sidebar">
        <PodcastSidebar podcast={state.podcast} onClick={setRedirect}></PodcastSidebar>
      </div>
      <div className="Episodes__Content">
        <EpisodeCard episode={episode}></EpisodeCard>
      </div>
    </div>
  );
};