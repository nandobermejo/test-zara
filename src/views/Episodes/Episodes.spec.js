import React from 'react';
import { mount } from 'enzyme';

import { Episodes } from 'views';
import { PodcastProvider } from 'context';

describe('Episodes', () => {
  it('should render without crashing', () => {
    const defaultProps = {
      match: { params: { podcastId: 1234, episodeId: 1234 } },
    };
    const component = mount(
      <PodcastProvider>
        <Episodes {...defaultProps}/>
      </PodcastProvider>
    );
    expect(component.find('.Loader')).toHaveLength(1);
  });
})

