import React, { useEffect, useContext, useState } from 'react';
import { Redirect } from 'react-router';

import './Details.scss';
import { PodcastSidebar, EpisodeList, Loader } from 'components';
import { PodcastContext } from 'context';

export const Details = (props) => {
  const { podcastId } = props.match.params;
  const { state, dispatch } = useContext(PodcastContext);
  const [ redirect, setRedirect ] = useState();

  useEffect(() => {
    dispatch({
      type: 'SELECT_PODCAST',
      id: podcastId
    });
    return () => dispatch({ type: 'UNSELECT_PODCAST'});
  }, [podcastId, dispatch]);

  if(!state.podcast) return <Loader />
  if(redirect) return <Redirect to={`/podcast/${podcastId}/episode/${redirect}`} />

  return (
    <div className="Details">
      <div className="Details__Sidebar">
        <PodcastSidebar podcast={state.podcast}></PodcastSidebar>
      </div>
      <div className="Details__Content">
        <EpisodeList list={state.podcast.episodes} onClick={setRedirect}></EpisodeList>
      </div>
    </div>
  );
};
