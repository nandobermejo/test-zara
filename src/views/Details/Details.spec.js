import React from 'react';
import { mount } from 'enzyme';

import { PodcastProvider } from 'context';
import { Details } from 'views';

describe('Details', () => {
  it('should render without crashing', () => {
    const defaultProps = {
        match: { params: { podcastId: 1234 } },
    };
    const component = mount(
      <PodcastProvider>
        <Details {...defaultProps}/>
      </PodcastProvider>
    );
    expect(component.find('.Loader')).toHaveLength(1);
  });
})

