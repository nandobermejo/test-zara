import React from 'react';
import { mount } from 'enzyme';

import { PodcastProvider } from 'context';
import { List } from 'views';

describe('List', () => {
  it('should render without crashing', () => {
    const component = mount(
      <PodcastProvider>
        <List />
      </PodcastProvider>
    );
    expect(component.find('.Message')).toHaveLength(1);
  });
})

