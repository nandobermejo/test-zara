import React, { useContext} from 'react';
import { Link } from 'react-router-dom';

import './List.scss';
import { PodcastContext } from 'context';
import { PodcastCard, InputSearch, Counter, Message } from 'components';

export const List = () => {
  const { state, dispatch } = useContext(PodcastContext);

  return (
    <div className="List">
      <div className="filterNav">
        <Counter count={state.filtered.length}></Counter>
        <InputSearch 
          placeholder="Filter podcasts..."
          onChange={(search) => dispatch({
            type: 'FILTER_LIST',
            search: search
          })}
        />
      </div>
      <div className="content">
        {
          state.filtered.length === 0 
          ? <Message text="No results found" />
          : state.filtered.map((item) => (
            <Link to={`/podcast/${item.id}`} key={item.id} className="item">
              <PodcastCard podcast={item}></PodcastCard>
            </Link>
          ))
        }
      </div>
    </div>
  );
};
