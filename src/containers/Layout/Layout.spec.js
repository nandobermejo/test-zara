import React from 'react';
import { shallow } from 'enzyme';

import { Header, Layout, Content } from 'containers';

describe('Layout', () => {
  it('renders without crashing', () => {
    const component = shallow(<Layout />);
    expect(component.find('.Layout')).toHaveLength(1);
  });

  it('shold render Header component as child', () => {
    const component = shallow(<Layout />);
    expect(component.find(Header)).toHaveLength(1);
  });

  it('shold render Content component as child', () => {
    const component = shallow(<Layout />);
    expect(component.find(Content)).toHaveLength(1);
  });
})

