import React from 'react';

import './Layout.scss';
import { Header, Content } from 'containers';

export const Layout = () => {
  return (
    <div className="Layout">
      <Header title="Podcaster" />
      <Content />
    </div>
  );
};
