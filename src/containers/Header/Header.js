import React from 'react';
import { Link, withRouter } from 'react-router-dom';

import './Header.scss';

export const Header = withRouter((props) => {
  const { title, history } = props;
  return (
    <header className="Header">
      <Link to="/">
        <div className="title">
          {title}
        </div>
      </Link>
      { history.action === 'POP' && <div className="live"></div>}
    </header>
  );
});