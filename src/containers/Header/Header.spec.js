import React from 'react';
import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router';

import { Header } from 'containers';

describe('Header', () => {
  it('renders without crashing', () => {
    const component = mount(
      <MemoryRouter>
        <Header />
      </MemoryRouter>
    );
    expect(component.find('.Header')).toHaveLength(1);
  });
})

