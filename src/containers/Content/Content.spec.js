import React from 'react';
import { shallow } from 'enzyme';

import { Content } from 'containers';

describe('Content', () => {
  it('renders without crashing', () => {
    const component = shallow(<Content />);
    expect(component.find('.Content')).toHaveLength(1);
  });
})

