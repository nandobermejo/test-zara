import React from 'react';
import { Switch, Route } from 'react-router-dom';

import './Content.scss';
import { List, Details, Episodes } from 'views';

export const Content = () => {
  return (
    <main className="Content">
      <Switch>
        <Route exact path="/" component={List} />
        <Route exact path="/podcast/:podcastId" component={Details} />
        <Route exact path="/podcast/:podcastId/episode/:episodeId" component={Episodes} />
      </Switch>
    </main>
  );
};
