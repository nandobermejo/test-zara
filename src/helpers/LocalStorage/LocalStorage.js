const expiration = 1000 * 60 * 60 * 24;

const setExpiry = (date) => date.getTime() + expiration;
const isExpired = (date) => date < new Date().getTime();

export const LocalStorage =  {
  get: (key) => {
    const record = JSON.parse(localStorage.getItem(key));
    if(!record) return;
    if(isExpired(record.expiration)){
      LocalStorage.delete(key);
      return;
    }
    return record.value;
  },
  set: (key, data) => {
    const record = {
      expiration: setExpiry(new Date()),
      value: data
    }
    localStorage.setItem(key, JSON.stringify(record,null,2))
  },
  delete: (key) => localStorage.removeItem(key)
};