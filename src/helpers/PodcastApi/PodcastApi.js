export const PodcastApi =  {
  getPodcasts: async () => {
    return await fetch(`${process.env.REACT_APP_API_URL}/podcasts`)
    .then(res => res.json())
    .then(data => data)
    .catch(err => err)
  },
  getPodcast: async (id) => {
    return await fetch(`${process.env.REACT_APP_API_URL}/podcasts/${id}`)
    .then(res => res.json())
    .then(data => data)
    .catch(err => err)
  }
};