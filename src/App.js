import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import './App.scss';
import { Layout } from 'containers';
import { PodcastProvider } from 'context';

export const App = () => {
  return (
    <BrowserRouter>
      <PodcastProvider>
        <Layout />
      </PodcastProvider>
    </BrowserRouter>
  );
}
